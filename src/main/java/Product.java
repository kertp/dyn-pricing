import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by kert on 19.12.15.
 */
class Product {
    private String name;
    private int quantity;
    private double currentPrice;
    private int validUntil;
    private boolean finished;

    private int initQuantity;
    private double initPrice;
    private int initValidUntil;

    private Lock currentPriceLock = new ReentrantLock();
    private Lock quantityLock = new ReentrantLock();
    private Lock validUntilLock = new ReentrantLock();
    private Lock finishedLock = new ReentrantLock();

    public Product(String name, int quantity, double currentPrice, int validUntil) {
        this.name = name;
        this.finished = false;

        this.initQuantity = quantity;
        this.initPrice = currentPrice;
        this.initValidUntil = validUntil;

        this.quantity = initQuantity;
        this.currentPrice = initPrice;
        this.validUntil = initValidUntil;
    }

    public int getValidUntil() {
        synchronized (validUntilLock){
            return validUntil;
        }
    }

    public void decrementValidUntil(int time) {
        synchronized (validUntilLock){
            this.validUntil -= time;
        }
    }

    public int purchase(int purchasedQuantity){
        synchronized (quantityLock){
            if(this.quantity < purchasedQuantity){
                purchasedQuantity = this.quantity;
            }
            this.quantity -= purchasedQuantity;
        }
        return purchasedQuantity;
    }

    public boolean isValid(){
        boolean time = this.getValidUntil() > 0;
        boolean productsLeft = this.getQuantity() > 0;
        return time && productsLeft;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        synchronized (quantityLock){
            return quantity;
        }
    }


    public double getCurrentPrice() {
        synchronized (currentPriceLock){
            return currentPrice;
        }
    }

    public void setCurrentPrice(double currentPrice) {
        synchronized (currentPriceLock){
            this.currentPrice = currentPrice;
        }
    }

    public boolean isFinished() {
        synchronized (finishedLock){
            return finished;
        }
    }

    public void setFinished(boolean finished) {
        synchronized (finishedLock) {
            this.finished = finished;
        }
    }

    public int getInitValidUntil() {
        return initValidUntil;
    }

    public int getInitQuantity() {
        return initQuantity;
    }

    public void reinit() {
        synchronized (quantityLock){
            quantity = initQuantity;
        }
        synchronized (currentPriceLock){
            currentPrice = initPrice;
        }
        synchronized (validUntilLock){
            validUntil = initValidUntil;
        }
    }
}
