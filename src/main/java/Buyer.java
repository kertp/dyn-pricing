import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by kert on 21.12.15.
 */

public class Buyer implements Runnable{

    DecimalFormat df = new DecimalFormat("#.##");

    private Product product;
    private double maxPrice;
    private int customersInTimeUnit;
    private double totalExpense;
    private final int step;
    private boolean finished;
    private Lock finishedLock;

    public Buyer(Product product, double maxPrice) {
        this.product = product;
        this.maxPrice = maxPrice;
        this.customersInTimeUnit = 10;
        this.totalExpense = 0;
        this.step = 20;

        this.finished = true;
        this.finishedLock = new ReentrantLock();
    }

    public void buy(Map<Integer, Double> prices){
        product.reinit();
        totalExpense = 0;

        Set<Integer> times = prices.keySet();
        List<Integer> timeList = new ArrayList<>(times);
        Collections.sort(timeList);
        Collections.reverse(timeList);
        int initTime = product.getValidUntil();
        int timeSpent = 0;
        for(Integer time : timeList){
            int multiplier = (initTime - time)/this.step;
            timeSpent += multiplier*this.step;
            if(multiplier <= 0){
                multiplier = 1;
            }
            initTime = time;
            double price = prices.get(time);
            product.setCurrentPrice(price);
            double pricePercentage = price / maxPrice;
            double purchasedQuantity = (this.getProbability(pricePercentage)*customersInTimeUnit);
            if(purchasedQuantity <= 0){
                purchasedQuantity = 1;
            }
            int purchased = product.purchase((int) Math.round(purchasedQuantity) * multiplier);
            totalExpense += purchased*price;
            System.out.println(timeSpent + " " + price + " " + df.format(totalExpense) + " " + product.getQuantity());
        }
        System.out.println(df.format(totalExpense) + " " + product.getQuantity());
    }

    public void buy() throws InterruptedException {
        while(!product.isFinished()){
            totalExpense = 0;
            while(product.isValid()){
                Thread.sleep(step);
                double currentPrice = product.getCurrentPrice();
                double pricePercentage = currentPrice / maxPrice;
                double purchasedQuantity = (this.getProbability(pricePercentage)*customersInTimeUnit);
                if(purchasedQuantity <= 0){
                    purchasedQuantity = 1;
                }
                int purchased = product.purchase((int) Math.round(purchasedQuantity));
                totalExpense += purchased*currentPrice;
            }
            if(totalExpense > 0){
                System.out.println("Total expense is:" + df.format(totalExpense) + ". Products left:" + product.getQuantity());
            }
            this.setFinished(true);
            synchronized (this) {
                this.wait();
            }
        }
    }

    public boolean isFinished() {
        synchronized (this.finishedLock){
            return finished;
        }
    }

    public void setFinished(boolean finished) {
        synchronized (this.finishedLock){
            this.finished = finished;
        }
    }

    public double getProbability(double price){
        return (1-Math.sqrt(price));
    }

    @Override
    public void run() {
        try {
            this.buy();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
