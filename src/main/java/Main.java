/**
 * Created by kert on 19.12.15.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        Product product = new Product("Milk", 500, 0.1, 2000);

        Buyer buyer = new Buyer(product, 3);
        Thread buyerThread = new Thread(buyer);
        buyerThread.start();

        Agent agent = new Agent(product, buyer);
        Thread agentThread = new Thread(agent);
        agentThread.start();

        Stock stock = new Stock(product, buyer, agent, 20);
        Thread stockThread = new Thread(stock);
        stockThread.run();
    }
}
