import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by kert on 21.12.15.
 */
public class Agent implements Runnable{

    DecimalFormat df = new DecimalFormat("#.##");

    private Product product;
    private double priceStep;
    private int buyerTimeStep;
    private Buyer buyer;
    public boolean finished;
    private Map<Integer,Map<Integer, Map<Double, Double>>> Q;
    private Lock finishedLock;

    final double alpha = 0.1;
    final double gamma = 0.9;

    private double maxReward = 0.0;
    private double bestPrice = 0.0;

    public Agent(Product product, Buyer buyer) {
        this.product = product;
        this.priceStep = 0.1;
        this.buyerTimeStep = 20;

        this.Q = new HashMap<>();
        this.buyer = buyer;

        this.finished = true;
        this.finishedLock = new ReentrantLock();
    }

    public double getReward(int initialStock, int newStock, double currentPrice, double timeLeft){
        int diff = initialStock - newStock;

        int periods = (int) (timeLeft / buyerTimeStep);
        int producstSold = periods*diff;
        double timeLeftPercentage = timeLeft/(double) this.product.getInitValidUntil();
        int productsLeft = (int) Math.round((this.product.getInitQuantity() * timeLeftPercentage)- producstSold);
        if(productsLeft <= 1){
            productsLeft = 1;
        }
        double income = (producstSold * currentPrice);
        return income;
    }

    @Override
    public void run() {
        try {
            this.updatePrice();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private double generatePrice(double currentPrice){
        Random random = new Random();
        int action = random.nextInt(2);
        if (action == 0 && currentPrice > priceStep) {
            currentPrice -= priceStep;
        } else if (action == 1) {
            currentPrice += priceStep;
        }
        if (currentPrice == 0) {
            currentPrice += priceStep;
        }

        return truncateTo(currentPrice, 2);
    }

    private void updatePrice() throws InterruptedException {
        while (!product.isFinished()) {
            while (product.isValid()) {

                double currentPrice = product.getCurrentPrice();
                int validUntil = product.getValidUntil();

                currentPrice = this.generatePrice(currentPrice);
                int initialStock = product.getQuantity();
                int newStock = initialStock;
                product.setCurrentPrice(currentPrice);

                while (newStock == initialStock && product.isValid()) {
                    newStock = product.getQuantity();
                }
                if(!(product.isValid())){
                    break;
                }

                double q = this.Q(validUntil, initialStock, currentPrice);
                double reward = this.getReward(initialStock, newStock, currentPrice, validUntil);
                double maxQVal = this.maxQ(validUntil, initialStock);
                double value = q + alpha * (reward + gamma * maxQVal - q);
                this.setQ(validUntil, initialStock, currentPrice, truncateTo(value, 2));
            }
            this.setFinished(true);
            synchronized (this) {
                this.wait();
            }
        }
        if(product.isFinished()){
            Map<Integer, Double> prices = this.bestPrices();
            buyer.buy(prices);
        }
    }

    private void setQ(int timeLeft, int stock, double price, double value) {
        Map<Integer, Map<Double, Double>> timeMap = this.Q.get(timeLeft);
        if(timeMap == null){
            timeMap = new HashMap<>();
            this.Q.put(timeLeft, timeMap);
        }

        Map<Double, Double> stockMap = timeMap.get(timeLeft);
        if(stockMap == null){
            stockMap = new HashMap<>();
            timeMap.put(stock, stockMap);
        }

        stockMap.put(price, value);
    }

    private Map<Integer, Double> bestPrices(){
        Set<Integer> keys = this.Q.keySet();
        List<Integer> keyList = new ArrayList<>(keys);
        Collections.sort(keyList);
        Collections.reverse(keyList);
        keyList.remove(0.0);
        Map<Integer, Double> bestPrices = new HashMap<>();
        for(Integer key:keyList){
            Map<Integer, Map<Double, Double>> timeMap = this.Q.get(key);
            Iterator it = timeMap.entrySet().iterator();
            double price = 0.0;
            double maxQ = -Double.MIN_VALUE;
            while(it.hasNext()){
                Map.Entry pair = (Map.Entry) it.next();
                Map stockMap = (Map) pair.getValue();
                Iterator sit = stockMap.entrySet().iterator();
                while(sit.hasNext()){
                    Map.Entry sitPair = (Map.Entry) sit.next();
                    double reward = (double) sitPair.getValue();
                    if(reward > maxQ){
                        maxQ = reward;
                        price = (double) sitPair.getKey();
                    }
                }
            }
            bestPrices.put(key, price);
        }
        return bestPrices;
    }

    private double maxQ(double timeLeft, int stock){
        Map<Integer, Map<Double, Double>> timeMap = this.Q.getOrDefault(timeLeft, new HashMap<>());
        double maxQ = 0.0;

        Map<Double, Double> stockMap = timeMap.getOrDefault(stock, new HashMap<>());

        Iterator it = stockMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry) it.next();
            Double value = (Double) pair.getValue();
            if(value > maxQ){
                maxQ = value;
            }
        }
        return maxQ;
    }

    private double Q(double timeLeft, int stock, double price) {
        Map<Integer, Map<Double, Double>> timeMap = this.Q.getOrDefault(timeLeft, new HashMap<>());
        Map<Double, Double> stockMap = timeMap.getOrDefault(stock, new HashMap<>());
        Double q = stockMap.getOrDefault(price, 0.0);
        return q;
    }

    public boolean isFinished() {
        synchronized (this.finishedLock){
            return finished;
        }
    }

    public void setFinished(boolean finished) {
        synchronized (this.finishedLock){
            this.finished = finished;
        }
    }

    static double truncateTo( double unroundedNumber, int decimalPlaces ){
        int truncatedNumberInt = (int)( unroundedNumber * Math.pow( 10, decimalPlaces ) );
        double truncatedNumber = (double)( truncatedNumberInt / Math.pow( 10, decimalPlaces ) );
        return truncatedNumber;
    }
}
