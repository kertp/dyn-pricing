/**
 * Created by kert on 22.12.15.
 */
public class Stock implements Runnable {

    private final Product product;
    private final Agent agent;
    private final Buyer buyer;
    private final int testTimes;

    public Stock(Product product, Buyer buyer, Agent agent, int testTimes) throws InterruptedException {
        this.product = product;
        this.buyer = buyer;
        this.agent = agent;
        this.testTimes = testTimes;
    }
    @Override
    public void run() {
        try {
            this.calculate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void calculate() throws InterruptedException {
        int step = 20;
        for(int i = 0; i < testTimes; i++){
            this.reinitProduct();
            while(product.isValid() && product.getQuantity() > 0){
                Thread.sleep((long) step);
                product.decrementValidUntil(step);
            }
        }
        product.setFinished(true);
        this.reinitProduct();
    }

    private void reinitProduct() throws InterruptedException {
        while(!this.agent.isFinished() || !this.buyer.isFinished()){
            Thread.sleep(20);
        }
        product.reinit();
        synchronized (agent){
            agent.notify();
            agent.setFinished(false);
        }
        synchronized (buyer){
            buyer.notify();
            buyer.setFinished(false);
        }
    }
}
